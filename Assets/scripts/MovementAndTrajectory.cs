﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

/// <summary>
/// Script attached automatically to each drone, displays the trajectory of the drone
/// </summary>
public class MovementAndTrajectory : MonoBehaviour
{
    List<Vector3> trajectory = new List<Vector3>();
    LineRenderer trajectory3D = new LineRenderer();
    Vector3[] save = new Vector3[10];
    public string nomObjet;
    //Renders and updates the trajectory
    Vector3[] TrajectoryRenderer(Vector3[] save)
    {
        Vector3[] tempo = InputManager.dicDrones[gameObject.name].ToArray();
        trajectory3D.SetVertexCount(tempo.Length);
        trajectory3D.SetPositions(tempo);
        for (int i = 0; i < tempo.Length; i++)
        {
            if (save[i] != tempo[i])
            {
                trajectory3D.SetPosition(i, tempo[i]);
            }
        }
        save = tempo;
        return save;
    }
    //Initialization of the trajectory
    void Start()
    {
        save = InputManager.dicDrones[gameObject.name].ToArray();
        trajectory3D = gameObject.AddComponent<LineRenderer>();
        TrajectoryRenderer(save);
        trajectory = InputManager.dicDrones[gameObject.name];
        trajectory3D.material = new Material(Shader.Find("Standard"));
        trajectory3D.material.color = Color.white;
        trajectory3D.startColor = Color.white;
        trajectory3D.endColor = Color.white;
        trajectory3D.startWidth = 0.05F;
        trajectory3D.endWidth = 0.05F;

    }

    void Update()
    {
        //Updates the trajectory with the information stored in the dictionary
        trajectory = InputManager.dicDrones[gameObject.name];
        if (InputManager.dicDrones.ContainsKey(gameObject.name))
        {
            trajectory[0] = InputManager.dicDrones[gameObject.name][0];
            gameObject.transform.position = trajectory[0];
            //gameObject.transform.position = Camera.main.transform.forward;
            save = TrajectoryRenderer(save);
        }
    }
}