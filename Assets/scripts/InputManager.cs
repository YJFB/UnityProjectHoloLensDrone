﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This script will get the information sent through the WebSocket and create a usable list from it
/// this list will be used in other scripts to give a trajectory to the drones
/// </summary>
public class InputManager : MonoBehaviour {
    public static List<List<string>> inputChunked = new List<List<string>>() ;
    public static Dictionary<string, List<Vector3>> dicDrones = new Dictionary<string, List<Vector3>>();
    string input;
    List<string> listInput = new List<string>();


    //Chunk the string received by the WebSocket into a list which elements are the drones' trajectories
    void  InputModifier(string input)
    {
        listInput = new List<string>(input.Split('|'));
        int len = listInput.Count;
        inputChunked = new List<List<string>>();
        foreach(string elem in listInput)
        {
            inputChunked.Add(new List<string>(elem.Split(':')));
            if (!dicDrones.ContainsKey(inputChunked[inputChunked.Count-1][0]))
            {
                dicDrones.Add(inputChunked[inputChunked.Count - 1][0], new List<Vector3>());
                CreateDrone(inputChunked[inputChunked.Count - 1][0]);
            }
        }
    }
    //Create a drone if one is added to the experiment
    void CreateDrone(string name)
    {
        Debug.Log(name + "dans IM");
        GameObject test = GameObject.CreatePrimitive(PrimitiveType.Cube);
        test.transform.localScale -= new Vector3(0.8F, 0.8F, 0.8F);
        test.name = name;
        test.transform.parent = GameObject.Find("groupDrones").transform;
        //test.AddComponent<FindDirection>();
        //test.AddComponent<Movement>();
        test.AddComponent<MovementAndTrajectory>();
        //test.GetComponent<Renderer>().material = GameObject.Find("Drone").GetComponent<Renderer>().material;

    }
	void Start () {

    }
	
	// Update is called once per frame
	void Update ()
    {
        input = gameObject.GetComponent<WebSocketClient>().reception;
        if (input.Length > 10)
        {
            if (input.Substring(0, 10) == "Trajectory")
            {
                input = input.Remove(0, 10);
                InputModifier(input);
            }
        }

    }
}
