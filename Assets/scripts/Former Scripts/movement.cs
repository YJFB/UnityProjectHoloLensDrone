﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This scripts manages the movement of drones in a simulation. It will not be useful in an experiment as positions are tracked in real time.
/// </summary>
public class Movement : MonoBehaviour {

    Vector3 direction;
    private Vector3 path;
    private Vector3 position;
    private float speedXZ = 0.7F;
    private float speedY = 0.7F;
    LineRenderer line;



    
	void Start () {
        //Get direction and position of the drone
        direction = gameObject.GetComponent<FindDirection>().direction;
        position = transform.position;
        //Create a line that shows the direction
        line = gameObject.AddComponent<LineRenderer>();
        line.SetPosition(0, position);
        line.SetPosition(1, position);
        line.material = new Material(Shader.Find("Standard"));
        line.material.color = Color.white;
        line.startColor= Color.white;
        line.endColor = Color.white;
        line.startWidth = 0.05F;
        line.endWidth = 0.05F;
    }
	
	void Update ()
    {
        //Update direction and position of the drone
        direction = gameObject.GetComponent<FindDirection>().direction;
        position = transform.position;
        if ((direction-position).magnitude>0.02)
        {
            //If the drone has a direction different from is position, it moves to this direction
            path = (direction - position).normalized;
            Vector3 pathXZ = new Vector3(path.x, 0, path.z);
            Vector3 pathY = new Vector3(0, path.y, 0);
            transform.Translate((pathY * speedY + pathXZ * speedXZ)/60);
            line.SetPosition(0, position);
            line.SetPosition(1, direction);

        
    }	
	}
}
