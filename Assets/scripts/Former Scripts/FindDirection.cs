﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;


public class FindDirection : MonoBehaviour
{
    public Vector3 direction;
    private Vector3 position;
    //Position and direction of the drone

    GameObject directionMarker;

    void Start()
    {
        direction = transform.position;
        position = transform.position;


        //Initialization of drone's position and direction and of camera's parametres

        directionMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        directionMarker.transform.position = position;
        directionMarker.transform.localScale -= new Vector3(0.95F, 0.95F, 0.95F);

        //Creation of a marker to show where the drone is going
        
    }

    void Update()
    {
        directionMarker.transform.position = direction;
    }

}
