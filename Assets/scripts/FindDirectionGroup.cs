﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

/// <summary>
/// Recognizes the order given by the user for it to be sent to the server.
/// </summary>
public class FindDirectionGroup : MonoBehaviour {

    public Vector3 direction;
    private Vector3 position;
    //Position and direction of the drone

    private Vector3 gazeDirection;
    private Vector3 cameraPosition;
    //Parametres of the camera

    private float distance = 2.5F;
    //Distance of the direction from the user

    GestureRecognizer gesture;

    RaycastHit hitInfo;

    GameObject selectedDrone=null;
    public static string mes="0";


    void Start()
    {
        direction = transform.position;
        position = transform.position;

        gazeDirection = Camera.main.transform.forward;
        cameraPosition = Camera.main.transform.position;

        //Initialization of drone's position and direction and of camera's parametres

        GameObject directionMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        directionMarker.transform.position = position;
        directionMarker.transform.localScale -= new Vector3(0.95F, 0.95F, 0.95F);

        //Creation of a marker to show where the drone is going

        gesture = new GestureRecognizer();


        gesture.Tapped += (s) =>
        {
            if (Physics.Raycast(cameraPosition, gazeDirection, out hitInfo))
            {
                //If we clicked on a drone, it becomes selected
                selectedDrone = hitInfo.collider.gameObject;
                GameObject testS = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                testS.transform.position = hitInfo.point;
                testS.transform.localScale -= new Vector3(0.95F, 0.95F, 0.95F);
                
             }

            else
            {
                //if we haven't clicked on a drone. The drone that is selected gets a new direction
                if (selectedDrone!=null)
                {
                    Debug.Log("Changement de mes");
                    Vector3 w= Camera.main.transform.forward;
                    mes =selectedDrone.name + ":" + w.x + ":" + w.y + ":" + w.z;
                }
            }

            //Moving the marker and affecting the desired direction in the direction variable
            //if this drone was selected
        };
    }

    void Update()
    {
        gazeDirection = Camera.main.transform.forward;
        cameraPosition = Camera.main.transform.position;
        gesture.StartCapturingGestures();
    }

}
