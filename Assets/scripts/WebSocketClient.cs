﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WebSocketSharp;
/// <summary>
/// WebSocketClient gets the coordinates from the GCS and sends orders to the drone through the GCS.
/// </summary>
public class WebSocketClient : MonoBehaviour {

    public string reception;
    WebSocket ws;
    UnityEngine.TouchScreenKeyboard keyboard;
    public string keyboardText = "";
    Action<bool> toto = (arg) => arg = true;



    int i = 0;
    //Connection routine
    void InitWS(string ip)
    {
        ws = new WebSocket("ws://" + ip + ":8080");

        ws.OnOpen += (sender, e) =>
            Debug.Log("Connected");

        ws.OnMessage += (sender, e) =>
        {
            reception = e.Data;
            if (ws.IsAlive || FindDirectionGroup.mes != "0")
            {
                try
                {

                    ws.SendAsync(FindDirectionGroup.mes, toto);
                    FindDirectionGroup.mes = "0";
                }
                catch
                {
                    Debug.Log("Error ");
                }
            }
        };
        ws.ConnectAsync();
    }
    void Start () {
        //Get the connection IP by a keyboard input
        keyboard = TouchScreenKeyboard.Open("10.0.0.5", TouchScreenKeyboardType.NumbersAndPunctuation, false, false, false, false, "Enter the IP:");
  
         }
    void Update()
    {
        if (keyboard.text != "")
        {

            InitWS(keyboard.text);
            keyboard.text = "";
        }

      
    }

}
