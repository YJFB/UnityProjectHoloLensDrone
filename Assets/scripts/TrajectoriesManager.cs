﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Stores the drones' trajectories in a static dictionary so it is available to each drone.
/// </summary>
public class TrajectoriesManager : MonoBehaviour {
    List<List<string>> realTimePositions = null;
    void NewPositions()
    {
        foreach (List<string> elem in  InputManager.inputChunked)
        {
            string[] droneAndPosition = { elem[0], elem[1] };
            realTimePositions.Add(new List<string>(droneAndPosition));
        }
    }
    // Dictionary update routine
    void TrajectoriesUpdate(List<List<string>> trajectories)
    {
        foreach (List<string> elem in trajectories) 
        {
            InputManager.dicDrones[elem[0]] = new List<Vector3>();
            List<Vector3> transition= new List<Vector3>();
            for(int i=1; i<elem.Count; i = i + 3)
            {
                transition.Add(new Vector3(float.Parse(elem[i]), float.Parse(elem[i + 1]), float.Parse(elem[i + 2])));
            }
            InputManager.dicDrones[elem[0]] = transition;
        }
    }

	void Update ()
    {
        TrajectoriesUpdate(InputManager.inputChunked);	
	}
}
